#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "pico/stdlib.h"
#include "hardware/irq.h"
#include "hardware/clocks.h"
#include "hardware/pio.h"
#include "encoder.pio.h"
#include "squarewave.pio.h"

static PIO PIO_O;    // pio object
static uint SM;      // pio state machine index
static uint PIO_IRQ; // NVIC ARM CPU interrupt number

static volatile uint32_t k_current_time = 0;
static volatile uint32_t k_prev_time = 0;

static volatile uint8_t k_current_phase = 0;
static volatile uint8_t k_prev_phase = 0;

static const uint8_t k_forward_direction[] = {0b00, 0b01, 0b11, 0b10, 0b00};
static const uint8_t k_reverse_direction[] = {0b00, 0b10, 0b11, 0b01, 0b00};

static volatile uint32_t k_ticks_counter = 0;

uint32_t k_meausure_time_us = 100;
uint32_t k_refresh_time_us = 1000000;

uint32_t k_prev_blink_time = 0;
uint32_t k_blink_time = 500000;
bool k_blink_state = false;

void pioIRQ()
{
    if (pio_interrupt_get(PIO_O, 0)) // returns TRUE if IRQ 3 is set
    {
        k_prev_time = k_current_time;
        k_current_time = time_us_32();
        k_prev_phase = k_current_phase;
        k_current_phase = pio_sm_get(pio0, 0);
        k_ticks_counter++;
        pio_interrupt_clear(PIO_O, 0); // clears the interrupt
    }
}

void encoder_program()
{
    PIO_O = pio0;         // Selects the pio instance (0 or 1 for pioNUM)
    PIO_IRQ = PIO0_IRQ_0; // Selects the NVIC PIO_IRQ to use

    uint offset = pio_add_program(PIO_O, &pio_encoder_program);
    uint encoder_pin_AB = 12;
    SM = pio_claim_unused_sm(PIO_O, true);
    pio_encoder_program_init(PIO_O, SM, offset, encoder_pin_AB);
    pio_set_irq0_source_enabled(PIO_O, pis_interrupt0, true); // sets IRQ0
    irq_set_exclusive_handler(PIO_IRQ, pioIRQ);               // Set the handler in the NVIC
    irq_set_enabled(PIO_IRQ, true);                           // enabling the PIO1_IRQ_0

    uint squarewave_pin = 10;
    PIO pio_instance = pio1;
    uint squarewave_offset = pio_add_program(pio_instance, &squarewave_program);
    uint sm = 0;
    squarewave_program_init(pio_instance, sm, squarewave_offset, squarewave_pin);
    pio_sm_set_enabled(pio_instance, sm, true);
}

void print_encoder_data()
{
    static bool encoder_direction = true;
    static uint32_t k_prev_print_time = 0;
    static uint32_t encoder_time_us = 0;

    if (time_us_32() - k_prev_print_time > k_refresh_time_us)
    {
        for (int i = 1; i < 5; i++)
        {
            if (k_forward_direction[i - 1] == k_current_phase && k_forward_direction[i] == k_prev_phase)
            {
                encoder_direction = true;
                break;
            }
            else if (k_reverse_direction[i - 1] == k_current_phase && k_reverse_direction[i] == k_prev_phase)
            {
                encoder_direction = false;
                break;
            }
        }

        irq_set_enabled(PIO_IRQ, false);
        encoder_time_us = k_current_time - k_prev_time;
        // printf("Direction: %s, num of ticks: %lu in period: %lu [us]\r\n", encoder_direction ? "clockwise" : "counterclockwise", num_of_ticks, meausure_time_us);
        printf("Direction: %s, time in us: %lu \r\n", encoder_direction ? "clockwise" : "counterclockwise", encoder_time_us); // Unlock this to meausure time between ticks

        // printf("Direction: %b \t Time in us: %ld \t Time per tick [us]: %lf \t Num of ticks: %ld\r\n", direction, (k_current_time - k_prev_time), (double)(time_us_32() - k_prev_print_time) / (double)k_ticks_counter, k_ticks_counter);
        irq_set_enabled(PIO_IRQ, true);
        k_ticks_counter = 0;
        k_prev_print_time = time_us_32();
    }
}

int main(void)
{
    const uint LED_PIN = PICO_DEFAULT_LED_PIN;
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    stdio_init_all();
    setup_default_uart();
    encoder_program();

    while (true)
    {
        if (time_us_32() - k_prev_blink_time > k_blink_time)
        {
            k_blink_state = !k_blink_state;
            gpio_put(LED_PIN, k_blink_state);
            k_prev_blink_time = time_us_32();
        }
        print_encoder_data();
    }

    return 0;
}